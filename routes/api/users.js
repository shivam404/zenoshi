const express = require('express');
var validator = require('validator');
const mysql = require('mysql');
var nodemailer = require('nodemailer');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const Tx = require('ethereumjs-tx');
var Web3 = require('web3');
//crypth private key
const Cryptr = require('cryptr');
const cryptr = new Cryptr('yuvraj_singh');

const router = express.Router();
var con = require("../../config/database");

const abiArray = '[{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"initialSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_value","type":"uint256"}],"name":"burn","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_subtractedValue","type":"uint256"}],"name":"decreaseApproval","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"}],"name":"balanceOf","outputs":[{"name":"balance","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_addedValue","type":"uint256"}],"name":"increaseApproval","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"},{"name":"_spender","type":"address"}],"name":"allowance","outputs":[{"name":"remaining","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"name":"previousOwner","type":"address"},{"indexed":true,"name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"burner","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Burn","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"owner","type":"address"},{"indexed":true,"name":"spender","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Transfer","type":"event"}]';

// contract Address
const contractAddress= "0x3484ec00f86f888ceaa129bfa6ff12611c8e3be5";


//Web3 initiated
//const precision = 1000000000000000000;
web3 = new Web3(new Web3.providers.HttpProvider("https://mainnet.infura.io/v3/02b94a9cba8946968af869c295993857"));

const validateRegisterInput = require('../../validation/register');
const validateLoginInput = require('../../validation/login');
const validateChnagePasswordInput = require('../../validation/change-password');
const precision = 1000000000000000000;


// @route   POST api/users/allTxnInfo
// @desc    Tests users route
// @access  Private
router.get(
    '/allTxnInfo',
    passport.authenticate('jwt', { session: false }),
    (req, response) => {
        let userId = req.user.id
        con.query('SELECT address,txninfo.to_add,txninfo.txn_date,txninfo.value,txninfo.txnhash from txninfo INNER JOIN address on address.user_id = txninfo.from_add', function (err, result){
            return response.status(200).json(result);
        })
})

// @route   POST api/users/getRecentTransaction
// @desc    Tests users route
// @access  Private
router.get(
    '/transferDesc',
    passport.authenticate('jwt', { session: false }),
    (req, response) => {
        let userId = req.user.id
        con.query('SELECT * FROM txninfo WHERE from_add = ' + userId, function (err, result){
            return response.status(200).json(result);
        })
})   

// @route   POST api/users/allWallet
// @desc    Tests users route
// @access  Private
router.get(
    '/allWallet',
    passport.authenticate('jwt', { session: false }),
    (req, response) => {
        let userId = req.user.id
        con.query('SELECT user.date_of_creation,user.email,user.type,address.address from user INNER JOIN address on address.user_id = user.id', function (err, result){
            return response.status(200).json(result);
        })
}) 

// @route   GET api/users/getTokenPrice
// @desc    Tests users route
// @access  Public
router.get('/getTokenPrice',
(req,response)=>{
    con.query('SELECT * FROM coin_price', function (err, result){
        return response.status(200).json(result[0]);
    })
})
// @route   POST api/users/getEtherBalance
// @desc    Tests users route
// @access  Private
router.post(
    '/transferToken',
    passport.authenticate('jwt', { session: false }),
    (req, response) => {
        let userId = req.user.id
        con.query('SELECT * FROM address WHERE user_id = ' + userId, function (err, result){
            var address = result[0].address;
            var privateKey = Buffer.from(result[0].pik.substring(2),'hex');
            var value=req.body.value;
            var toAddress = req.body.to;
            var desc = req.body.description;

            var abi=JSON.parse(abiArray);
            const contractInstance=new web3.eth.Contract(abi,contractAddress);
            var res = contractInstance.methods.balanceOf(address).call().then(function(data){

            var bald = data;
            var pres = contractInstance.methods.decimals().call().then(function(data){
                    var finalbal = bald/Math.pow(10,data);
                    console.log(finalbal)
                    if(finalbal > value){
                        var count;
                        // get transaction count, later will used as nonce
                        web3.eth.getTransactionCount(address).then(function(v){
                            console.log("Count: "+v);
                            count = v;
                            var amount = web3.utils.toHex(1*1e8);
                            console.log(amount)
                            //creating raw tranaction
                            var rawTransaction = {"from":address, "gasPrice":web3.utils.toHex(10 * 1e9),"gasLimit":web3.utils.toHex(95000),"to":contractAddress,"value":"0x0","data":contractInstance.methods.transfer(toAddress, amount).encodeABI(),"nonce":web3.utils.toHex(count)}
                            console.log(rawTransaction)
                            //creating tranaction via ethereumjs-tx
                            var transaction = new Tx(rawTransaction);
                            //signing transaction with private key
                            transaction.sign(privateKey);
                            //sending transacton via web3js module
                            web3.eth.sendSignedTransaction('0x'+transaction.serialize().toString('hex'))
                            .on('transactionHash',(txnHash)=>{
                                var insertAddress = "INSERT INTO txninfo set ?";
                                var insertAddressJson = {
                                    from_add: userId,
                                    to_add : toAddress,
                                    txnHash:txnHash,
                                    description:desc,
                                    value:value
                                }
                                console.log(insertAddressJson)
                                con.query(insertAddress, insertAddressJson , function (err, result) {  
                                    return response.status(200).json({'status':'success','txnHash':txnHash})
                                }) 
                            });
                        })
                    }else{
                        return response.status(400).json({error:'Insufficient balance'});
                    }
                });
            })
})

})

// @route   POST api/users/getEtherBalance
// @desc    Tests users route
// @access  Private
router.post(
    '/getEtherBalance',
    passport.authenticate('jwt', { session: false }),
    (req, response) => {
    const address=req.body.address;
    var balance = web3.eth.getBalance(address).then(function(result){
        response.json({"balance":result/precision});
    });
})

// @route   POST api/users/getTokenBalance
// @desc    Tests users route
// @access  Private
router.post(
    '/getTokenBalance',
    passport.authenticate('jwt', { session: false }),
    (req, response) => {
    var abi=JSON.parse(abiArray);
    const account=req.body.address;
    const contractInstance=new web3.eth.Contract(abi,contractAddress);
    var res = contractInstance.methods.balanceOf(account).call().then(function(data){

        var bald = data;
        var pres = contractInstance.methods.decimals().call().then(function(data){
                var finalbal = bald/Math.pow(10,data);
                return response.json({"balance":finalbal})
            });

    });
})

// @route   POST api/users/signup
// @desc    Tests users route
// @access  Public
router.post('/signup', (req, res) => {
    console.log(req);
    const { errors, isValid } = validateRegisterInput(req.body);
    // Check Validation
    if (!isValid) {
        return res.status(400).json(errors);
    }

    checkEmailExistQuery = 'SELECT * FROM user WHERE email = ' + mysql.escape(req.body.email);
    con.query(checkEmailExistQuery, function (err, result) {
        if (err) throw err;
        if(result.length  == 1){
            errors.email = 'Email already exists';
            return res.status(400).json(errors);
        }else{
            const newUser = {
                email: req.body.email,
                password: req.body.password,
                type:req.body.type,
                status: (req.body.type == 'a') ? 1 : 0
            };
            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(newUser.password, salt, (err, hash) => {
                    if (err) throw err;
                    newUser.password = hash;
                    var InsertSql = "INSERT INTO user set ?";
                    con.query(InsertSql, newUser , function (err, result) {
                        if (err) throw err;
                        //send verification mail
                        let randomHashed = randomHash(8);
                        (req.body.type == 'a') ? '' : mail(newUser.email,randomHashed);
                        //generate address using web3
                        var pass = 'testing';
                        var account = web3.eth.accounts.create();
                        var address=account.address;
                        var prik = account.privateKey;
                        var pk = Buffer.from(prik.substr(2),'hex');
                        var data = web3.eth.accounts.encrypt(prik,pass);
                        //end of generating//
                        var userId = result.insertId;
                        var privatekey = cryptr.encrypt(prik);
                        var insertAddress = "INSERT INTO  address set ?";
                        var insertAddressJson = {
                            user_id: userId,
                            address : address,
                            pik:'0x'+privatekey
                        }
                        con.query(insertAddress, insertAddressJson , function (err, result) {   
                            if (err) throw err;
                            const userData = {
                                user_id: userId,
                                hash: randomHashed
                            };
                            var emailSql = "INSERT INTO email_verify set ?";
                            con.query(emailSql, userData , function (err, result) {
                                if (err) throw err;
                                return res.status(200).json({'emailId':result.insertId});
                            })
                        })
                    });
                });
            });
        }
    });
});


// @route   POST api/users/verifypasscode
// @desc    Tests users route
// @access  Public

router.post('/verifypasscode', (req, res)=>{
    errors = {};
    const emailId = req.body.emailId;
    const emailHash = req.body.emailHash;

    checkEmailVerify = 'select * from email_verify where id = ' + mysql.escape(emailId); 
    con.query(checkEmailVerify, function(err, userResult) {
        if (err) throw err;
        if(userResult.length  != 1){
            errors.error = 'Email id not found';
            return res.status(400).json(errors);
        }
        let hash = userResult[0].hash;
        if(hash != emailHash){
            errors.error = 'Email hash not found';
            return res.status(400).json(errors);
        }
        let userId = userResult[0].user_id;

        updateEmailStatus = 'update user set status = 1 where id = ' + mysql.escape(userId); 
        con.query(updateEmailStatus, function(err, userData) {
            if (err) throw err;
        })
        
        updateEmailStatus = 'update email_verify set status = 1 where user_id = ' + mysql.escape(userId);
        con.query(updateEmailStatus, function(err, userData) {
            if (err) throw err;
        })

        getUserData = 'select * from user where id = ' + mysql.escape(userId); 
        con.query(getUserData, function(err, userData) {
            if (err) throw err;
            const payload = { id: userData[0].id, email: userData[0].email }; // Create JWT Payload
            // Sign Token
            jwt.sign(
                payload,
                'shivamsingh',//security key
                (err, token) => {
                res.json({
                    success: true,
                    token: 'Bearer ' + token
                });
                }
            );
        })
    })
})



// @route   POST api/users/login
// @desc    Tests users route
// @access  Public

router.post('/login', (req, res) => {
    const { errors, isValid } = validateLoginInput(req.body);
    // Check Validation
    if (!isValid) {
        return res.status(400).json(errors);
    }

    const email = req.body.email;
    const password = req.body.password;
    const type = req.body.type;
    checkEmailExistQuery = 'SELECT * FROM user WHERE email = ' + mysql.escape(email) +' and type= '+ mysql.escape(type);
    con.query(checkEmailExistQuery, function (err, userResult) {
        if (err) throw err;
        if(userResult.length  != 1){
            errors.email = 'Email not found';
            return res.status(400).json(errors);
        }else{
            let userId = userResult[0].id;
            con.query('SELECT * FROM user WHERE id = ' + userId, function (err, result){
                let status = result[0].status;
		console.log(result);
                if(status == 0){
                    errors.email = 'Please verify your email';
                    return res.status(400).json(errors);
                }else{
                    bcrypt.compare(password, userResult[0].password).then(isMatch => {
                        if (isMatch) {
                            // User Matched
                            const payload = { id: userResult[0].id, email: userResult[0].email }; // Create JWT Payload
                            // Sign Token
                            jwt.sign(
                                payload,
                                'shivamsingh',//security key
                                (err, token) => {
                                res.json({
                                    success: true,
                                    token: 'Bearer ' + token
                                });
                                }
                            );
                        }else {
                            errors.email = 'Password incorrect';
                            return res.status(400).json(errors);
                        }
                    })
                }
            })
        }
    });
})
// @route   POST api/users/current
// @desc    Return current user
// @access  Private

router.post(
    '/change_password',
    passport.authenticate('jwt', { session: false }),
    (req, res) => {
        let error = {};
        let userId = req.user.id
        const { errors, isValid } = validateChnagePasswordInput(req.body);
        // Check Validation
        if (!isValid) {
            return res.status(400).json(errors);
        }
        let oldpassword = req.body.oldpassword;
        con.query('SELECT * FROM user WHERE id = ' + userId, function (err, result){
            bcrypt.compare(oldpassword, result[0].password).then(isMatch => {
                if(isMatch){
                    bcrypt.genSalt(10, (err, salt) => {
                        bcrypt.hash(req.body.newpassword, salt, (err, hash) => {
                            if (err) throw err;
			    let sql = "update `user` set `password` = '" + hash + "' where id =" + userId + ";";
                            con.query(sql, function(err, result){
                                if (err) throw err;
                                error.success = 'Password change successfully';
                                return res.status(200).json(error);
                            })
                        })
                    })
                }else{
                    errors.oldpassword = 'old password did not match';
                    return res.status(400).json(errors);
                }
            })
        })
    }
);


// @route   GET api/users/useraddress
// @desc    Tests users route
// @access  Private

router.post(
    '/useraddress',
    passport.authenticate('jwt', { session: false }),
    (req, res) => {
        let userId = req.user.id
        con.query('SELECT address FROM address WHERE user_id = ' + userId, function (err, result){
            if (err) throw err;
            return res.status(200).json(result[0]);
        })
});

//function for sending mail

function mail(email,text){
    var transporter = nodemailer.createTransport({
    host: 'smtp.sendgrid.net',
    port:587,
    auth: {
        user: 'apikey',
        pass: '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'
    }
    });
    var mailOptions = {
        from: 'shvmsngh99@gmail.com',
        to: email,
        subject: 'Sending Email using Node.js',
        text: 'Your email verification passcode is ' + text
    };
    
    transporter.sendMail(mailOptions, function(error, info){
    if (error) {
        console.log(error);
    } else {
        console.log('Email sent: ' + info.response);
    }
    });
}
function randomHash(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < length; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}

module.exports = router;
