const express = require('express');
const bodyParser = require('body-parser');
const passport = require('passport');
const users = require('./routes/api/users');
var cors = require('cors')
const app = express();
app.use(cors())
//Passport middleware
app.use(passport.initialize())

//Passport config
require('./config/passport')(passport);

// Body parser middleware
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

app.get('/', (req, res) => res.send('Hello World'));

// Use Routes
app.use('/api/users', users);


const port = process.env.PORT || 3032;

app.listen(port, '172.105.235.156',() => console.log(`Server running on port ${port}`));
// npm run server
